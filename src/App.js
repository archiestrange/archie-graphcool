import React, { Component } from 'react';
import logo from './logo.svg';
import ListPage from './components/ListPage'
import { HttpLink } from 'apollo-link-http';
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import './App.css';

const httpLink = new HttpLink({ uri: 'https://api.graph.cool/simple/v1/cjucieo2r0hk30117luv5b6yx' })

const client = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache()
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <ListPage />
          </header>
        </div>
      </ApolloProvider>
    );
  }
}

export default App;
